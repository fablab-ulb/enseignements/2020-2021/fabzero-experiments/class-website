## Class schedule

Thursday **10h30-12h30** and **13h30-15h30** at FabLab ULB.

*important note : The topic of each class is subject to change.*

Feb 11 - Introduction to Frugal Science, Digital Fabrication, Project Management and Documentation  
Feb 18 - Compliant Mechanisms & Flexures, Computer-Aided Design  
Feb 25 - 3D Printing  
Mar 04 - Computer Controlled Cutting  
Mar 11 - Electronic 1 - Prototypage Production     
Mar 18 - Electronic 2 - Fabrication  

Mar 25 - Final Project Development - Engage  
Apr 01 - Final Project Development - Investigate    
Apr 22 - Final Project Development - Act     
Apr 29 - Final Project Development   
May 06 - Final Project Development  
May 07 - Final Project Development   

Jun 15 - Final Project Presentation (10h-13h)

## Deadlines :  

Apr 29 - Modules complete (1st round)  
May 06 - Cross-evaluation complete  
May 14 - response to evaluation and modules final updates  
