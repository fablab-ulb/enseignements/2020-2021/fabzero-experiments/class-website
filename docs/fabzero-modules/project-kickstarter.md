# Kickstarting your project

## Goal of this unit

The goal of this unit is to define and develop your final project that solves something.

## Class Materials

* [Challenge-based learning](https://www.challengebasedlearning.org/framework/)

## Challenge-based learning framework

The Challenge Learning Framework is divided into three interconnected phases: Engage, Investigate and Act.

**Engage** - *Find a challenge that motivates you*

* **Big ideas** - *What is the broad theme or concept I would like to explore ?*
* **Essential questions** - *What are the essential questions that reflect personal interests and the needs of the community ?*
* **Challenge** - *what is your call for action ?*  



**Investigate** - *step on the shoulders of the giants not on their toes*

* **Guiding questions** - *what are all the questions that needs to be answered in order to tackle the challenge ? Priority ?*
* **Guiding activities/ressources** - *What resources can I use to answer those questions ? Science.*
* **Analysis and synthesis** - *write a summary of your findings, facts and data collected*  



**Act** - *Develop a solution, implement it and get feedback*

* **Solution concepts** - *What is your solution about ?*
* **Solution development** - *how do this solution solve the challenge ? Prototype and develop.*
* **Implementation and evaluation** - *Experiment and evaluate the solution.*  

## Additional advices

* Don't use this framework linearly. Rapid prototype and make mini investigations. **Fail quickly !**
* Spiral Management. Don't try to save the world at your first try. Start with something small and expand from it (spiral).
* One way to find a disruptive solution is to navigate back and forth from the concepts space to the knowledge space until you find THE solution ! ([C-K theory](https://www.ck-theory.org/la-theorie-ck/))
* Question the obvious !!! And learn about the [illusion of explanatory depth](https://www.edge.org/response-detail/27117). You can find sublime in the mundane !
* Find and ask mentors !

## Assignment

* Reflect, document and share your process on your final project page.
