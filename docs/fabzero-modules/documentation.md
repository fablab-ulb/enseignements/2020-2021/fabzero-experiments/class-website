# Documenting your work

## Goal of this unit

The goal of this unit is to learn some easy but powerful tools to write better documentation and share it using version control systems.

## Class materials

### Mind map

<embed src="../docs-tube.pdf?zoom=100" width="100%" height="500px">
</embed>

### Quick links

* [Command Line Interface CLI](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/command-line.md)
* [Docs](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#why-is-documentation-important-)
* [GitLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#gitlab)
* [Git](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#git)

* [Access to this FabZero-Experiments class space on GitLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments)

## Assignment

* work through a git tutorial
* use git to build a personal site in the class archive describing you and draft your idea board for your final project
* document your learning path for this module.
