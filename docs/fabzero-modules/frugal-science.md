# Frugal Science

## Goal of this unit

The goal of this class is to learn to **design**, **fabricate** and **document** a scientifically-based and frugal project to solve a problem that you have identified and care about.


## Links

* [How To Make (almost) Any Experiment / FabZero Inside](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/)
* [Denis Terwagne's blog](https://dtwg4.github.io/blog-flexible-jekyll/entrepreneuriat-social-et-science-frugale/)
* [BioE271: Frugal Science, Stanford University, USA](https://www.frugalscience.org/)
* [FabLab Studio, ULB, Belgium](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/)


## Assignment

* Draft your personal idea board on your final project page
* For each idea identify
    * Why do you care about this problem ? (Anecdote, ...)
    * What is the problem statement (without thinking of any solution) ?
    * Document some data and scale on the problem.
