# Final Project

## Assignment

You will design, fabricate and document a scientifically-based and frugal solution to solve a real-world problem you have identified.

* You will position your project along the [17 Sustainable Development Goals](https://sdgs.un.org/) listed by the United Nations.
* You will show scientific evidence with sources and references on which you build on your project.
* You will use at least one digital fabrication technique that is used in a fablab.


## Preparation for the final presentation

For the final presentation, you will prepare

* a graphical abstract illustrating your question and project (1920x1080px firstname.surname-slide.png image).
* a pecha-kucha (20 slides, 20 seconds / slide) to share your project with an interdisciplinary audience (firstname.surname-presentation.pdf).
* a technical document (poster,...) presenting the scientific aspects of your project (at least a QR code linking to your online documentation).
* prototypes that illustrate your process and your final achievement.
