# 3D printing

## Goal of this unit

In this unit, you will learn to

* Identify the advantages and limitations of 3D printing.
* build on other people's code and give proper credits.
* Apply design methods and production processes using 3D printing.


## Class materials

* [Compliant Mechanisms & flexures](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/3492f57983b21259d80457abc2f78fa86a1e6420/vade-mecum/compliant-mechanisms.md)
* [3D scanner demo](https://fabacademy.org/2020/labs/ulb/students/quentin-bolsee/projects/final-project/3-development/)
* [3D printing](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md)

## Assignment

* Complete your FlexLinks kit that you made by fetching the code of at least 2 parts made by other classmates during week 2. Acknowledge their work. Modify their code and get the parts ready to print.
* Test the design rules for your 3D printer(s).
* 3D print your flexible construction kit, make a compliant mechanism out of it with several hinges that do something,
* Make and post a (compressed <10 Mo) video of your working mechanism.

## Learning outcome

* Identify the advantages and limitations of 3D printing.
* build on other people's work and give proper credits.
* Apply design methods and production processes to show your understanding of 3D printing.
