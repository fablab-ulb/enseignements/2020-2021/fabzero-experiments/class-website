# Project Management

## Goal of this unit

The goal of this unit is to learn some tips for successful Project Management

## Project Management Techniques

* Modular & Hierarchical Planning
* Supply-side time management VS task management
* Spiral Development
* Parallel Development
* Triage
* Bottom-up Debugging
* As-You-Work Documentation

Learn more [here](http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/projMgmt/)

### Some more :

* Parkinson Law
  > *“Work expands so as to fill the time available for its completion.”*

* Hofstadter’s Law
  > *“it always takes longer than expected, even taking into account Hofstadter’s Law.”*

## Assignment

* test and apply those principles all along the class
