# Electronic Prototyping

## Goal of this unit

In this unit, you will learn to use a microcontroller development board to prototype electronic applications by programming it and using input and output devices.

## Class materials

* [IO modules listing](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/56a8960118866d055633035714c50a5ee2434985/IO-Modules.md)
* [Arduino Uno Pinout](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/56a8960118866d055633035714c50a5ee2434985/img/Arduino-uno-pinout.png)
* [DFS 2017 - Sensors Workshop](https://dfs2017.blogspot.com/search/label/A2%20-%20%20Tutoriel%20senseur)
* [Projet CO2 - Mesurer le CO2 pour mieux aérer](http://projetco2.fr/)

## Assignment

* Make a basic exercice using a development board ([examples here](https://www.arduino.cc/en/Tutorial/BuiltInExamples))
* Measure something: add a sensor to a development board and read it.
* Make your board do something : add an output device to a development board and program it to do something
