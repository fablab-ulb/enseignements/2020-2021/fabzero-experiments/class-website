# Final Projects

## Assignment

You will design, fabricate and document a scientifically-based and frugal solution to solve a real-world problem you have identified.

* You will position your project along the [17 Sustainable Development Goals](https://sdgs.un.org/) listed by the United Nations.
* You will show scientific evidence with sources and references on which you build on your project.
* You will use at least one digital fabrication technique that is used in a fablab.

## Final projects documentation

| | |
| --- | --- |
|![Théo Lisart](img/theo.lisart-slide.png) |[**The kitchen microscope**](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/theo.lisart/#Project/fabzero04/) <br> [*Théo Lisart*](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/theo.lisart/) |
|![Paul Bryssinck](img/paul.bryssinck-slide.png) |[**UV et Fast Fashion**](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/projet/finalProject-Redaction/) <br> [*Paul Bryssinck*](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/) |
|![Simone Vitale](img/simone.vitale-slide.png) |[**Plastic recycling**](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/project/project/) <br> [*Simone Vitale*](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/) |
|![majda.ghalem-slide.png](img/majda.ghalem-slide.png) |[**Recyclage créatif**](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/majda.ghalem/final-project/) <br> [*Majda Ghalem*](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/majda.ghalem/) |
|![doriane.galbez-kristine.valat-slide.png](img/doriane.galbez-kristine.valat-slide.png) |[**Une nourriture saine pour les étudiants**](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/final-project/) <br> *[Doriane Galbez](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/) & [Kristine Valat](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/kristine.valat/)*|
|![stephaniefloriane.krinsweyer-slide.png](img/stephaniefloriane.krinsweyer-slide.png) |[**STEAM Lab**](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/final-project/) <br> *[Stéphanie Krins](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/) & [Floriane Weyer](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/floriane.weyer/)* |

## Preparation for the final presentation

For the final presentation, you will prepare

* a graphical abstract illustrating your question and project (1920x1080px firstname.surname-slide.png image).
* a pecha-kucha (20 slides, 20 seconds / slide) to share your project with an interdisciplinary audience (firstname.surname-presentation.pdf).
* a technical document (poster,...) presenting the scientific aspects of your project (at least a QR code linking to your online documentation).
* prototypes that illustrate your process and your final achievement.
